import React from 'react';
import Styles from './pantsScreen.module.css';
import PantsData from '../../data/pants.json';
import GridItem from '../gridItem';
import gsap from 'gsap';

class PantsScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      items: ''
    }
  }

  componentDidMount() {
    document.getElementsByTagName('title')[0].innerHTML = 'Pants | Siyousen';

    gsap.to('.gridContainer', {
      opacity: 1,
      duration: 0.5
    })

    this.setState({
      items: PantsData.map((item, index) => {
        return (<GridItem key={index} index={index} item={item} category="pants" history={this.props.history} />)
      })
    })
  }

  componentWillUnmount() {
  }

  render() {
    return (
      <div className={Styles.mainContainer}>
        <div className={`row m-0 gridContainer ${Styles.gridContainer}`}>
          {this.state.items}
        </div>
      </div>
    )
  }
}

export default PantsScreen;
