import React from 'react';
import Styles from './headbandsScreen.module.css';
import HeadbandsData from '../../data/headbands.json';
import GridItem from '../gridItem';
import gsap from 'gsap';

class HeadbandsScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      items: ''
    }
  }

  componentDidMount() {
    document.getElementsByTagName('title')[0].innerHTML = 'Headbands | Siyousen';

    gsap.to('.gridContainer', {
      opacity: 1,
      duration: 0.5
    })

    this.setState({
      items: HeadbandsData.map((item, index) => {
        return (<GridItem key={index} index={index} item={item} category="headbands" history={this.props.history} />)
      })
    })
  }

  componentWillUnmount() {
  }

  render() {
    return (
      <div className={Styles.mainContainer}>
        <div className={`row m-0 gridContainer ${Styles.gridContainer}`}>
          {this.state.items}
        </div>
      </div>
    )
  }
}

export default HeadbandsScreen;
