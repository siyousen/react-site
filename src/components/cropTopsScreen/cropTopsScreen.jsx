import React from 'react';
import Styles from './cropTopsScreen.module.css';
import CropTopsData from '../../data/cropTops.json';
import GridItem from '../gridItem';
import gsap from 'gsap';

class CropTopsScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      items: ''
    }
  }

  componentDidMount() {
    document.getElementsByTagName('title')[0].innerHTML = 'Crop Tops | Siyousen';

    gsap.to('.gridContainer', {
      opacity: 1,
      duration: 0.5
    })

    this.setState({
      items: CropTopsData.map((item, index) => {
        return (<GridItem key={index} index={index} item={item} category="cropTops" history={this.props.history} />)
      })
    })
  }

  componentWillUnmount() {
  }

  render() {
    return (
      <div className={Styles.mainContainer}>
        <div className={`row m-0 gridContainer ${Styles.gridContainer}`}>
          {this.state.items}
        </div>
      </div>
    )
  }
}

export default CropTopsScreen;
